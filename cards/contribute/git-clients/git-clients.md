---
mount: contribute/git-clients
name: "Git clients"
tags:
  - contribute/git
redirects:
  - contribute:git-clients
  - /external/contribute/git-clients/
  - /cards/contribute:git-clients
  - /external/cards/contribute:git-clients
  - /external/external/contribute/git-clients/
---

# Git clients

This card recommends known clients for the git version control systems.

Installation instructions are provided [in a separate card](/contribute/install-git).

## Suggested GUI clients

GUI clients allow you to run git actions without the need to type into a terminal:

* [Sourcetree](https://www.sourcetreeapp.com)
* [Fork](https://git-fork.com)
* [SmartGit](https://www.syntevo.com/smartgit/)

You can find more clients on the [Git webpage](https://git-scm.com/downloads/guis).

## IDEs

Many integrated development environments (IDEs) come with the git functionality included:

* [GitLab WebIDE](https://docs.gitlab.com/ee/user/project/web_ide/)
* [Visual Studio Code](https://code.visualstudio.com/)
* [Atom](https://atom.io/)
* [IntelliJ](https://www.jetbrains.com/idea/)
* [PyCharm](https://www.jetbrains.com/pycharm/)

## Miscellaneous tools

* [Meld](https://meldmerge.org/) the conflict-resolution utility
* [gitg](https://gitlab.gnome.org/GNOME/gitg) for repository browsing and viewing, well integrated to Ubuntu desktop
* [gource](https://gource.io/) for animated history visualizations
