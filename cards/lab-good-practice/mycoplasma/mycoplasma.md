---
mount: lab/mycoplasma
name: "Mycoplasma contamination check"
tags:
  - lab/good-practice
redirects:
  - lab:mycoplasma
  - /external/lab-good-practice/mycoplasma/
  - /cards/lab-good-practice:mycoplasma
  - /external/cards/lab-good-practice:mycoplasma
  - /external/external/lab-good-practice/mycoplasma/
  - /cards/lab:mycoplasma
  - /external/cards/lab:mycoplasma
---


# Mycoplasma contamination check

Mycoplasma is a genus of bacteria that belongs to the class Mollicutes. What sets mycoplasmas apart from many other bacteria is their lack of a cell wall. The absence of a rigid cell wall makes them resistant to certain antibiotics that target cell wall synthesis. Meaning that even if you have antibiotics in your cell culture media, you can have a mycoplasma contamination.

They are among the smallest free-living organisms. Their small size, along with their lack of a cell wall, makes them challenging to study using conventional microscopy.

Mycoplasma can alter many aspects of their host culture's cell, from cell metabolism to chromosomal aberrations, everything is included. Having undetected mycoplasma in your cell culture can easily falsify your research results and so invalidate your complete work.

Due to these reasons, mycoplasma is one of the most serious and devastating culture contaminants.

**Because they are difficult to detect, a cell culture good practice is to regularly check for the absence of mycoplasma, even in cultures that do not seem to be contaminated.**

### Sources of mycoplasma contamination

The most common cause for mycoplasma contamination is the presence of another infected cell line in the laboratory. Sharing the same hood, associated with gaps in cell culture practices will lead to the spreading of the contamination between cell cultures.

Another important cause is the cell culturist. Talking and sneezing can generate significant amounts of aerosols that have been shown to contain mycoplasma. Additionally, dirty lab coats can be a source of contamination when a dust-laden sleeve is put into a laminar flow hood and dust particles fall into cultures.
However, good aseptic techniques and training do significantly reduce the risk of contamination via this route.

## Mycoplasma detection

There are two basic testing methods for mycoplasma:

1. Direct culture in media
2. Indirect tests that measure specific characteristics of mycoplasma

The easiest way of detection is to use a commercially available mycoplasma detection kit. They are, for example, based on PCR, staining or specific biochemical assays.

### Mycoplasma Control Kits

If you are not sure what test methods or kits to use, please do not hesitate to ask your colleagues at the LCSB.

Here are some examples of kits used by LCSB researchers:

- MycoAlert Mycoplasma Detection Kit *Westburg*
- MycoAlert Control Set *Westburg*
- Mycoplasma Detection Kit *InvivoGen*
- Lookout Mycoplasma PCR Detection KIT *Sigma Aldrich*
- Universal Mycoplasma Detection kit *ATCC*

For more information on these products refer to Quarks.

### Control Intervals

The longer the intervals between tests, the more research work will be put under question and will need to be repeated in case of contamination.

The LCSB, therefore, recommends testing cell cultures for mycoplasma contaminations **at time of arrival** and afterwards at **monthly intervals**.
You do not only prevent your own research results to be invalidated, but help your colleagues' cell cultures to remain free of mycoplasma contaminations.
