---
mount: exchange-channels/owncloud-transfer
name: "Data Transfer with OwnCloud"
tags:
  - exchange-channels/transfer
redirects:
  - integrity:transfer:owncloud-privatebin
  - /external/integrity/transfer/owncloud-privatebin/
  - /cards/integrity/transfer:owncloud-privatebin
  - /external/integrity/transfer:owncloud-privatebin
  - /external/external/integrity/transfer/owncloud-privatebin/
---

# Data Transfer with OwnCloud
This How-to Card provides a guide on how to transfer data (< 4 GB) with OwnCloud by creating a password-protected and time-restricted link. The card additionally describes security measures to implement when transferring Sensitive Human Data with OwnCloud. Please note that [LCSB File Transfer (LFT)](/exchange-channels/lft/) is strongly advised in the latter case.

If you wish to share Human Data **recurrently** with external collaborators, it is recommended to use Owncloud with Cryptomator for encryption of data on the cloud. For further information, please visit the dedicated card on [Cryptomator](/exchange-channels/cryptomator).

## Step-by-step guide on transferring data with OwnCloud
**Prerequisite:** a LUMS account is needed and can be requested via the [Service Portal](https://service.uni.lu/sp?id=sc_cat_item&table=sc_cat_item&sys_id=c536257ddb336010ca53454039961936).

1. Go to [OwnCloud](https://owncloud.lcsb.uni.lu/) and login with your LUMS account.
2. Create a folder in your local owncloud space and upload the data for transfer. 
    * For sensitive data, it is strongly advised to encrypt the data before uploading to OwnCloud. In this case, go [to the section below](#transferring-sensitive-data-via-owncloud) before proceeding.
3. Share the folder by pressing the "share" button as shown below.
      ![owncloud-share-button.png](img/owncloud-share-button.png)
4. Sharing panel will open. Select “public links”.
      ![owncloud-public-link.png](img/owncloud-public-link.png)
5. Give the link a proper name. 
6. Select access mode. It is recommended to choose the minimum required to ensure data integrity (e.g. data accuracy, unintended modifications to data, etc.).
7. Generate a secure password. Some browsers can automatically generate one for you, or use any password manager.
8. Set an appropriate expiration date. Recommendation is 1 week, but can be extended in special cases.
9. Type the recipient's e-mail(s).
10. *OPTIONAL:* Tick the box "Send the copy to self".
11. Use [PrivateBin](https://privatebin.lcsb.uni.lu/) link for sharing password securely and add to message.
      ![set-password-expiration-date.png](img/set-password-expiration-date.png)
12. Press share button. 

## Transferring sensitive data via OwnCloud:
![owncloud-privatebin.png](img/owncloud-privatebin.png) 

1. Use an archiver software, which supports AES256 encryption. 
   * *Windows:* use [7-zip](https://www.7-zip.org/download.html).
   * *Mac:* use [Keka](https://www.keka.io/en/) and enable AES256 encryption by going to `settings > compression` and tick the **Use AES-256 encryption** checkbox like displayed below.
     ![choosing encryption in keka](img/keka-encryption.png){.w1-2}
2. Use a password generator to generate a strong encryption password.
   * E.g. a password manager like [BitWarden](https://bitwarden.com/) or [KeePass](https://keepass.info/).
3. Go to [LCSB PrivateBin](https://privatebin.lcsb.uni.lu/) and paste the password in the Editor tab.
4. Enable the feature "Burn after reading" by ticking the checkbox as shown below. This means that the password link can only be used **once** as it expires upon first access.
   ![encryptionpassword.png](img/encryptionpassword.png)
   * You will be promted to enter your LUMS credentials once you click on "Send".
   * You should be redirected to a page containing the password link.
     ![passwordLink.png](img/passwordLink.png)
5. Share the password link with your collaborator via your preferred communication channel.
6. The collaborator (recipient) **must** confirm that the password was successfully received before proceeding. If collaborator reports an error, it indicates the password was compromised and data transfer is not secured anymore. In this case the zipped archive should be deleted and the process should started again. 
7. Upload the encrypted archive to the OwnCloud folder. 
8. Share the OwnCloud access link with your collaborator by following [the steps above](#data-transfer-with-owncloud). 
9. The collaborator can now decrypt the archive with the password received via PrivateBin.
