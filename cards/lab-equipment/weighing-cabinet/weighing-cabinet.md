---
mount: lab/weighing-cabinet
name: "Safety Weighing Cabinet: Utilization"
tags:
  - lab/equipment

revised:
  by: Sandy Thill
  date: 2024-12-04

---


# Safety Weighing Cabinet: Utilization

A Satorius SWC1200TNF safety weighing cabinet (LCSB02251) is installed in BT2-E01-109.

It has to be used to weight dangerous and volatile compounds. For more information on the balance, refer to the corresponding [How-to card](/lab/balances).

![img1](img/img1.jpg){.w3-4 .align-center}

## Personal Protective Equipment (PPE)

PPE to be worn when operating the weighing cabinet:

- Safety glasses
- Two pairs of nitrile gloves
- Lab coat

![img15](img/img15.png){.w2-3 .align-center}

## Safety Measures

1. The weighing cabinet must always remain turned on.
The green lamp next to the on/off button should be illuminiated green.

![img3](img/img3.jpg){.w2-5 .align-center}

Note: Should the cabinet exceptionally be turned off, check out the below [section](#turn-on) on how to turn it on again.

2. [Only use the weighing cabinet when both airflow LEDs on the control interface are green!]{.color-red}  
In case of acoustic alarm, contact instrument care via the [ticketing system](https://service.uni.lu/sp?id=sc_cat_item&sys_id=be0873f8db7070505c72ef3c0b96199f&sysparm_category=af924f17dbac38105c72ef3c0b96194b).

![img2](img/img2.jpg){.w2-5 .align-center}

::: danger-box
[Alarm ]{.box-header}

Do not use the cabinet with the alarm in operation or if the alarm displays an airflow fault condition.
:::

3. Fill out the log sheets after each use to track chemicals and maintain a clean and safe environment for the next user.


## Waste

A waste bag is present in the hood (on the right side).
When used, please remove it when you finish and discard it in the correct waste container.

![img4](img/img4.jpg){.w2-3 .align-center}


### How to safely remove the waste disposal bag

**Preparation:**

- Wear double gloves before initiating the removal procedure.
- Ensure the following items are readily available inside the enclosure:
  - Replacement waste bag
  - Elastic band to secure to disposal chute

![img6](img/img6.jpg){.w1-3 .align-center}

You can take them from the spare parts box:

![spare](img/spare.jpg){.w1-3 .align-center}

**Safe Change Procedure:**

For a better understanding of the below described procedure, please have a look on the following video:

![waste-disposal.mp4](https://webdav.lcsb.uni.lu/public/R3lab/howto-files/weighing-cabinet-bag-disposal.mp4){.w1-2 .align-center}


1. Remove and seal the internal bag containing waste. 

![img8](img/img8.jpg){.w1-3 .align-center}

![img9](img/img9.jpg){.align-center}

2. Push the sealed bag into the outer bag, ensuring proper containment.

![img9a](img/img9a.jpg){.w1-3 .align-center}

3. Attach a new waste bag to the internal disposal chute section, positioning the bag to face into the enclosure. 

![img10](img/img10.jpg){.align-center}

4. Remove outer gloves and keep them in the weighing cabinet to avoid contamination. 

![img11](img/img11.jpg){.w1-3 .align-center}

5. Remove the external bag and seal it securely. 

![img12](img/img12.jpg){.w2-3 .align-center}

6. Attach a new waste bag to the disposal chute and fix it in place with the **same** black elastic band. 

![img13](img/img13.jpg){.w2-3 .align-center}

7. Push the new internal bag through to the disposal chute and dispose of the contaminated outer gloves inside the new waste bag.

![img14](img/img14.jpg){.w1-3 .align-center}

**Waste disposal:**

Dispose of the used waste bag in a CW17 container.

![img7](img/img7.jpg){.w1-3 .align-center}


## Cleaning

Always keep the cabinet clean to ensure proper use and to avoid potential cross-contaminations. 

For cleaning, use the antistatic decontamination wipes present next to the weighing cabinet.

![img5](img/img5.jpg){.w1-3 .align-center}

### Spill

In case of spill, follow the procedure in the spill kit available next to the hood.

![spill-kit](img/spill-kit.jpg){.w1-2 .align-center}

## How to turn on the safety cabinet{#turn-on}

- Upon activation, the unit checks both left and right-hand sensors, displaying their status (e.g., "LEFT OK").
- If a fault is detected, the display will indicate the sensor name and "faulty" until the fault is cleared.
- Wait for successful completion of memory test and initialization before proceeding.

## Documentation of weighed products

Please fill the sheets available next to the hood with what compounds have been weighed.

![paper](img/paper.jpg){.w1-2 .align-center}