---
mount: lab/freezers
name: "-20°C freezers: maintenance"
tags:
  - lab/equipment
redirects:
  - lab:freezers
  - /external/lab-equipment/freezers/
  - /cards/lab-equipment:freezers
  - /external/cards/lab-equipment:freezers
  - /external/external/lab-equipment/freezers/
  - /cards/lab:freezers
  - /external/cards/lab:freezers
---


# -20°C freezers: maintenance

Routine maintenance of fridges and freezers prevents from issues and preserves integrity of samples.

In the event of a fridge or a freezer would get damaged or have a failure due to a lake of maintenance on the user side, the cost of the repair or of the replacement will be charged to the group in charge of the device.

The fridges and freezers are under constant temperature monitoring. In order to avoid alarms, the monitoring has to be disconnected before the cleaning of 4°C fridges and the defrosting of -20°C/-80°C freezers (see [How to deactivate the alarm in Sensor4Lab](#how-to-deactivate-the-alarm-in-sensor4lab)).

**Please inform the Instrument Care team before every kind of maintenance by sending [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=6ea39fdbdbec38105c72ef3c0b96196a&sysparm_category=af924f17dbac38105c72ef3c0b96194b) 24h before starting your maintenance**.

Always start your maintenance in the morning.

When the maintenance is finished and the temperature is back to normal, reply on the open ticket to let them know you are done.

## How to deactivate the alarm in Sensor4Lab

All the freezers are monitored via a software called Sensor4Lab. To avoid any alarm due to the defrosting, the first step of the defrosting process is always to put the monitoring on hold. This step is needed for the cleaning of 4°C fridges and defrosting of -20°C/-80°C freezers.

**Only technicians have access to Sensor4lab**. Please ask a technician of your team to disable the sensor associated to the fridge you want to clean or defrost.
-	If you’re a technician, go on [Sensor4lab](https://sensor4lab.lcsb.uni.lu/account/logon) and login with your credentials received from the Instrument Care Team. If you don’t have any credentials yet, please send [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=dc205fd7dbec38105c72ef3c0b96191d&sysparm_category=af924f17dbac38105c72ef3c0b96194b)
-	Please follow the process explained [here](https://dutycall.lcsb.uni.lu/cards/alarm-management/deactivate-alarm/)

Please note that there is a 30 minutes delay between the moment that you disable the sensor and the response of the system. This means that you should disable the monitoring 30 min before you start the maintenance.

## How to defrost a -20°C freezer

After some time and depending of its opening frequency, ice can be formed inside of the freezers. To ensure a good maintenance, a reliable temperature and to keep the energy consumption as low as possible, this ice has to be removed on a regular basis. It is on the responsibility of the lab users to remove the excess of ice.

At LCSB, we have different kind of -20°C freezers. The defrosting procedure is the same for all of them, except for the Liebherr LGPv. Those freezers defrost automatically, and therefore, the user does not need to do it.

### LIEBHERR LGPv (600 L)

**no defrosting needed**

![img8.png](img/img8.png)

### Other freezers

**Every month**: check if ice has been formed. Defrost if needed.

**Every three months**: we recommend to defrost the -20°C every three months. THis period can vary depending on the frequency of opening the device.

**Every six months** for the other freezers

![img9.png](img/img9.png)

-	24h before your maintenance, inform the Instrument Care team by sending [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=6ea39fdbdbec38105c72ef3c0b96196a&sysparm_category=af924f17dbac38105c72ef3c0b96194b) and [disable the Rmoni sensor](#how-to-deactivate-the-alarm-in-Sensor4Lab)
-	Switch off the appliance and pull out the main plug
-	Remove the drawers and transfer their content to another freezer
-	To speed up the defrosting process, a beaker containing warm, but not boiling, water can be put on one of the cooling plates
-	Leave the door open for defrosting
-	Check regularly and mop the melted water
-	Once all the ice melted and the water has been mopped, clean the inside of the freezer with lukewarm water and mild detergent. **Ensure that no cleaning water penetrates into the electrical components or ventilation grids**
-	Dry completely the inside of the freezer
-	Reconnect the main plug and switch the freezer back on
-	Wait until the pre-set temperature is reached before transferring the content of the freezer back
-	Enable the Rmoni sensor and inform the Instrument Care team that you are finished and that everything is back to normal
-	Fill the maintenance form located on the door of the freezer
