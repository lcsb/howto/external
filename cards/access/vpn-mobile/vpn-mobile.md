---
mount: access/vpn-mobile
name: "VPN connection on your mobile phone"
tags:
  - access
redirects:
  - access:vpn-mobile
  - /external/access/vpn-mobile/
  - /cards/access:vpn-mobile
  - /external/cards/access:vpn-mobile
  - /external/external/access/vpn-mobile/

revised:
    by: Sandy Thill
    date: 2025-01-10
---


# VPN connection on your mobile phone

## iPhone
1. In the App store, download "Cisco Secure Client".

    ![img1.png](img/img1.png){.w1-2}

2. Click on *Connections*.

    ![img2.png](img/img2.png){.w1-2}

3. Click on *Add VPN Connection...*.

    ![img3.png](img/img3.png){.w1-2}

4. In Server Address, write *vpn.uni.lu/MFA* and save.

    ![img4.png](img/img4_mfa.png){.w3-4}

5. Allow the *AnyConnect* to add VPN configurations.

    ![img5.png](img/img5.png){.w1-2}

6. Enter your UL credentials.

    ![img6.png](img/img6.png){.w1-2}

7. To turn on/off the VPN, slide the button *AnyConnect VPN*.

    ![img7.png](img/img7_mfa.png){.w3-4}

Everytime you want to turn the VPN on, you will need to authenticate yourself (for example via Microsoft authenticator app).