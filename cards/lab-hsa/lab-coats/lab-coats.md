---
mount: lab/lab-coats
name: "Lab coats"
tags:
  - lab/hsa
  - lab/ppe
redirects:
  - lab:lab-coats
  - /external/lab-hsa/lab-coats/
  - /cards/lab-hsa:lab-coats
  - /external/cards/lab-hsa:lab-coats
  - /external/external/lab-hsa/lab-coats/
  - /cards/lab:lab-coats
  - /external/cards/lab:lab-coats
---


# Lab coats

Which lab coat should I wear? Where do I get a new lab coat and where do I dispose a dirty one?

## Wear the right lab coat in the right area

![1.png](img/1.png)

### Lab coat mandatory

![2.jpg](img/2.jpg)

- All BSL1 and BSL2 laboratories
- Storage Rooms : Chemical Storage/ cryostorage/LCSB stock
For all users, visitors, technicians.

### Lab coat forbidden

![3.png](img/3.png)

- Offices and open offices
- Kitchen, Coffee Points (red flooring)
- Toilets

### Lab coat allowed

![4.png](img/4.png)

- Lift
- Staircases
- Corridor to the stairs/lift
- Corridor to the support team zone BT2 1st floor (trolleys/ fridges) __BUT not the support team offices__

## Where can I find a new clean lab coat?

### BT1

Clean lab coats are stored in the cupboard of the room BT1-E05-502, on the 5th floor. They are sorted by Biosafety Level (BSL1 and BSL2) and sizes. For the larger sizes, you can find them with extra lengths on the sleeves (+15cm).

### BT2

Clean lab coats are stored in the cupboards of the MUF room, on the 1st floor, close to the Support Biotech Team offices. They are sorted by Biosafety Level (BSL1 and BSL2) and sizes. For the larger sizes, you can find them with extra lengths on the sleeves (+15cm).

## What should I do with my dirty lab coat?

On  a regular basis you should bring your lab coat in the tissue bags close to the vending machine on the ground floor in BT1 or BT2.

Remove the pins and empty the pockets.

Someone from the Support Biotech Team will take care to send the lab coats for cleaning.

![7.jpg](img/7.jpg){.w3-4}

## Pins
To identify your lab coat as yours, do not forget to pin your pin on it.

Create a [ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=050f18e08788855007eeed309bbb35b6&sysparm_category=cca7f2c1db683c905c72ef3c0b961940) to request your pins.

## Help needed?

Any question regarding lab coats, goggles or other PPE, [please send a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=f140d745dbe83c905c72ef3c0b96194e&sysparm_category=cca7f2c1db683c905c72ef3c0b961940).
