---
mount: lab/safety-glasses
name: "Safety Glasses"
tags:
  - lab/ppe
---

# Safety Glasses


Wearing safety glasses protects your eyes from accidental droplets and splashes of hazardous substances (chemical or biological).
They also keep out dust and other small particules that might irritate your eyes.
Safety glasses help us to avoid serious eye injuries and protect your eyesight for the long term.

Over the last years, several lab accidents at the university involved splashes that hit the eyes. 
The university has therefore decided to make it mandatory to wear safety glasses in all laboratories (exception rodent facility and zebrafish facility room S01-004).


## How to get safety glasses

### For staff

A pair of safety glasses is given out by the support team (to be picked up in the support team office). 
Treat this pair of glass carefully, if you lose or break it, your PI will have to finance your new ones.

Should you forget your glasses, or you work sometimes in a different building, it is possible to get visitor glasses at the reception (for a transient time).

When leaving the LCSB, please return your safety glasses to the support team.

### For visitors

Visitors can get safety glasses at the receptions of BT1 and BT2.
They have to return them before leaving the building.
There are return boxes available at the receptions.

In case you have visitors outside the normal opening hours of the reception, please contact the support team, in advance, to organize the safety glasses.

## Types of safety glasses

- For people not wearing correction-glasses, the following model is available:

 ![img24.jpg](../ppe/img/img24.jpg){.w2-5 .align-center}

- For people wearing correction-glasses, we have two models of cover-glasses they can choose from (depending on the form of their glasses).

::: centered-block
 ![img2](img/img2.jpg){.w2-5}
 ![img3](img/img3.jpg){.w2-5}
:::


#### Safety glasses with eye-correction

Under some conditions, it is possible to get safety glasses with eye-correction.
This has to be discussed case by case; for this please open a [ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=f59d2f21833d4250a4e796efeeaad392&sysparm_category=cca7f2c1db683c905c72ef3c0b961940).  
Note that this process can take quite some time.



## How to clean my safety glasses

 We recommend using ethanol to clean your safety glasses.

