---
mount: lab/shipment
name: "Shipment of biological or chemical samples with carrier"
tags:
  - lab/hsa
  - lab/transport
redirects:
  - lab:shipment
  - /external/lab-hsa/shipment/
  - /cards/lab-hsa:shipment
  - /external/cards/lab-hsa:shipment
  - /external/external/lab-hsa/shipment/
  - /cards/lab:shipment
  - /external/cards/lab:shipment
revised:
  by: Fasavanh Sanichanh
  date: 2025-02-24
---


# Shipment of biological or chemical samples with carrier

The Support Biotech Team at LCSB organises the biological or chemical samples shipments with carriers. They have been trained and certified by IATA (International Air Transportation Association) to be allowed to prepare the shipping documents for dangerous goods.

For shipments of documents or non-lab related goods, please contact the administrative assistants.

## 1) Prepare the package

A **triple packaging** is necessary for any transport outside the building, by air or by road.

- **First packaging** refers to your initial container, eg the Eppendorf, Falcon tube or culture plate. The tubes or plates should be closed with parafilm to avoid any leakage.

- **Secondary packaging** is ensured when you pack the first packaging in a plastic bag. Plastic bag of different sizes are available in the Support Biotech Team office. When preparing your package please make sure to add enough absorbing material in the secondary packaging.

- **Third packaging** is the box used for transport. It can be a styrofoam or cardboard box sealed with tape. You can find tape in the Support Biotech Team office.

*Tips*
- Dedicated bags can be used for the transport of biological material.
- Dedicated box can be used for the transport of chemicals.
- Do not overlap the labels.
- Do not change the size of the labels provided to you.
- The tape should not hide the labels.

![triple-packaging.mp4](https://webdav.lcsb.uni.lu/public/R3lab/howto-files/triple-packaging.mp4)

## 2) Request a shipment with a carrier

Please send a ticket a few days in advance via [Service Portal](https://service.uni.lu/sp?id=sc_cat_item&sys_id=9cf7f205db683c905c72ef3c0b9619cd&sysparm_category=ca67f2c1db683c905c72ef3c0b9619a8).

### Select a carrier

#### DHL or Fedex
- Transit time within European Union: 1 or 2 days
- Transit time outside European Union: up to 5 days

If the samples must be kept frozen, please fill the package with dry ice (at least 5kg for EU shipment and 15kg for non EU shipment). We recommend to place an order of [dry ice](/lab/dryice) in Quarks by Wednesday at the latest. The deliveries of dry ice occur every Monday.

Shipping days:
- European Union: Monday, Tuesday or Wednesday for Room Temperature shipment
- European Union: Monday or Tuesday for shipment with dry ice
- Outside European Union: Monday for a delivery by Friday. In case the package must be delivered on Monday we can schedule a shipment on Thursday or Friday (the package will be in transit all weekend long)

*Please note these are recommendations and delays at the customs might occur.*


#### Quickstat or Barbican Logistics
If you would like to ship precious samples we recommend Quickstat or Barbican Logistics as carrier. It is an expensive service but they offer the guarantee to maintain your samples at the right temperature. (e.g.: if the samples must be kept frozen they can come with a styrofoam box full of dry ice and refill it until the delivery. A secondary packaging of your samples is sufficient.)

*Please note this process must be prepared few weeks in advance. A quotation must be requested by the Lab Procurement Team then a Purchase Order must be issued.*

### Complete all information
If an information is missing, the shipment might be postponed.

The Support Biotech Team will review the information provided in the ticket and will make sure all the safety requirements are fullfiled (shipping temperature and nature of the goods). They will provide the shipping documents and labels to be enclosed to the package and give you all the instructions to follow in the ticket.

# Reception of biological or chemical samples
In case a collaborator needs to ship samples to LCSB and we have to cover the shipping costs, the Support Biotech Team can organise the shipment to LCSB.

Please send a ticket a few days in advance via [Service Portal](https://service.uni.lu/sp?id=sc_cat_item&sys_id=6a3e4701dbe83c905c72ef3c0b961903&sysparm_category=ca67f2c1db683c905c72ef3c0b9619a8).

*Please note we can also provide our DHL or Fedex account number to the collaborators in case they would like to organise the shipment. They must add the OTP in reference.*

