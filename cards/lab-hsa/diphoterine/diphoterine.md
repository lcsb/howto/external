---
mount: lab/diphoterine
name: "Diphoterine: utilization"
tags:
  - lab/hsa
  - lab/emergency
redirects:
  - lab:diphoterine
  - /external/lab-hsa/diphoterine/
  - /cards/lab-hsa:diphoterine
  - /external/cards/lab-hsa:diphoterine
  - /external/external/lab-hsa/diphoterine/
  - /cards/lab:diphoterine
  - /external/cards/lab:diphoterine
---


# Diphoterine: utilization

Diphoterine is a decontamination solution used in first aid for the emergency treatment of chemical spills to the eyes and body.

Diphoterine solution contains an amphoteric, chelating molecule: a substance which is capable of reacting with both acids and alkalis when applied to either type of chemical spill, stopping the aggressive action of a corrosive or irritant chemical, halting the reaction with the body.

**Please note that diphoterine is not effective against hydrofluoric acid (HF).**

**In case of skin or mucosa contact with a chemical, always ask for help, ask someone to call 112 and 5555 and use Diphoterine!**

**The helper should call 112 and 5555 and indicate as much details as possible abot the substance splashed (what, how much, chemical or biological).**

**In case of chemical contact, the helper has to download the Safety Data Sheet (SDS) from Quarks and the victim has to have it with him to go to the hospital.**

You can find the Diphoterine  station (orange box) in the Safety points in BT1 and in the laboratories in BT2

![img2.jpg](img/img2.jpg)

## User guide

![img6.png](img/img6.png)

![img5.png](img/img5.png)
