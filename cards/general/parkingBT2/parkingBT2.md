---
mount: general/parkingBT2
name: "Parking in BT2"
order: 300
tags:
  - general

---

# Parking in BT2

## Parking Subscription Requirement
All vehicles parked in the BT2 building basement must have a valid parking subscription in FIORI via [My Mobility Subscription Requests](https://fiori.uni.lu/fiori#MobilitySubscription-create&/Parking/0050569a-630e-1ed9-b084-04b337efc0dd).

## Exceptions

There are a few exceptions to the parking subscription requirement:

- **Service Car:** You can use the service car for approved purposes, see the [related How-to Card](/lab/electric-car)
- **Pre-booked Visitor Parking:** Visitors with a pre-booked parking space in Archibus are permitted to park in the designated area.
- **Sample/Material Transport:** Staff transporting samples or heavy materials to/from another UNI building are allowed temporary parking for loading and unloading purposes.
- **On-Call duty assigned people:** the weekly designed staff can park during his/her on-call week out-off the working time (6pm to 8am & weekend) using the duty-call card.
- **Authorized Substitute Parking:** Staff can park in another staff member's space with a valid subscription, following the proper authorization procedure outlined below. 

***Please note that the guard will not open the gate if the below process was not followed.***

### Authorized Substitute Parking: Obtaining Proper Authorization:

1.	**Contact the Subscription Holder:** Reach out via email to the staff member with a valid parking subscription and request permission to use their booked spot on a specific date.
2.	**Include Loge & Disaching Emails in CC:** Always include the BT2 loge email address (<loge.bt2@uni.lu>) and the Dispatching Securite (<dispatching.securite@uni.lu>) in your authorization request email.
3.	**Verify Loge & Dispaching Email adresses in Reply:** When the staff member replies, ensure the Loge and the Dispaching email addresses remain in the "CC" field.
4.	**Guard Updates Calendar:** The loge will add your name to their calendar for authorized parking.

**For Regular Usage:**

If you plan to regularly use a colleague's parking space (e.g., every Monday), clearly specify this in your authorization request email. 
The loge will then add this information to the calendar for ongoing reference.

By following these guidelines, we can maintain a fair and organized parking system for everyone in the BT2 building.

**Note:** if the guard notices that this process is not followed and/or 2 persons with an agreement are parked at the same time, he will ask the person without a proper authorization to remove his car from the parking.

## Where to find the University Parking spots

### -01


![img1.png](img/img1.png)

### -02


![img2.png](img/img2.png)
