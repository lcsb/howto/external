---
mount: general/get-to-LCSB
name: "How to get to the Luxembourg Centre for Systems Biomedicine"
order: 200
tags:
  - general
redirects:
  - general/getToLCSB
  - general:getToLCSB
  - /external/general/getToLCSB/
  - /cards/general:getToLCSB
  - /external/cards/general:getToLCSB
  - /external/external/general/getToLCSB/
---

# How to get to the Luxembourg Centre for Systems Biomedicine

![](img/LCSB.jpg){.align-center}

| House of Biomedicine (BT1)     | Biotech II (BT2)               |
| ------------------------------ | ------------------------------ |
| 7, avenue des Hauts-Fourneaux, L-4362 Esch-sur-Alzette  | 6, avenue du Swing, L-4371 Belvaux |
| N 49°30'5.62'', E 5°56'57.14'' | N 49°29'59.73'', E 5°57'7.271  |

## 1. Arriving by plane at Luxembourg airport
The Luxembourg airport, called Findel, is 34 km away from the University Campus BELVAL where the LCSB is located.
You can take either taxi or public transport to reach the LCSB.
- **Via Taxi (110€ one way)**

*Price: ca 87-110€ one way (in Luxembourg taxis are very expensive!)*
*Duration: 30 minutes (during rush hour 45-60 min)*
Trustworthy taxi services: [webtaxi](https://www.webtaxi.lu) (tel: +352 27 515); [coluxtaxi](http://colux.lu) (tel: +352 48 22 33). Please ask confirmation of the price.
A map of the Campus BELVAL and how to find all LCSB buildings is shown below.

- **Via public transport (free)**

*Public transportation is free in Luxembourg!*
*Duration: 60 minutes (including bus, train and walk)*
*We recommend using the Mobiliteit.lu mobile App or [website](https://www.mobiliteit.lu), to plan your public transport in Luxembourg.*

From Luxembourg airport you can take the following bus lines to reach train stations connected to Belval:
Through Luxembourg Central Station:
- Line 29, direction 'Hesperange, Cité Um Schlass', get off at 'Luxembourg, Gare Centrale'.
- Line 223, direction 'Bonnevoie, Lycée Bouneweg PE', get off at 'Luxembourg, Gare Rocade'.


Through Howald train station:
- Line 302, direction 'Howald, Ronneboesch', get off at 'Howald, Ronneboesch'.

From these trainstations, you can take a train towards Belval every 20 minutes (see below).  

## 2. Arriving by train or bus at Luxembourg central station

*Public transportation is free in Luxembourg!*
*Duration: 35 minutes (including train and walk)*
*We recommend using the Mobiliteit.lu mobile App or [website](https://www.mobiliteit.lu), to plan your public transport in Luxembourg.*


Take the train in direction of 'Rodange' or 'Petange'. Your stop is called: 'Belval-Université' station. You can take the same trains in 'Howald' and 'Bettembourg'.

A map of the Campus BELVAL and how to find both LCSB buildings is shown below. From Belval train station it's about a 5 minutes-walk to either of our locations.

You can also arrive by bus from Luxembourg City. Please visit [www.mobiliteit.lu](https://www.mobiliteit.lu/en/journey-planner/) to select your travelling options. *Please note, however, that the train is the preferred option.*

## 3. Arriving by car

*Duration: about 20 minutes from Luxembourg City or 30 minutes from Luxembourg Airport (without traffic jam).*

If you travel by car, take the A4 towards Esch-sur-Alzette and stay on this road until it joins the B40. Follow the signs 'Belval - Uni'. This will take you onto the 'Porte de France', the main road into Esch-Belval.<br>
Please use the plan below to orientate on the campus. The Biotech I (BT1) building is directly next to the old blast furnace site, close to the red Dexia buildings. The building is a bit hidden. It is a six floor modern white building. Biotech II (BT2) is close to ALDI, right next to ADEM and across the street from Southlane Towers, with the main entrance to Avenue du Swing.

The entrance of the Ketterthill building, which is the west-arm of the building complex, is in the back of the building. The LCSB staff has offices on the 5th floor.

The Rouden Eck (or BTL) has its entrance to the Boulevard du Jazz (No. 1), between AXA and Lineheart, close to the bus stop Um Bedding. You can find LCSB staff on floors 4 and 5. 

### Parking at Campus BELVAL
Since Campus Belval is foreseen to be a green car-free Campus, parking may not be easy. Due to the constructions around
our buildings, we currently cannot offer visitor parking. To park your car we recommend using one of the following. From
either site it is less than five minute walk to either of our buildings.
- University parking “Mason du Savoir” has outdoor and indoor areas.
Price: first 15 min free, first 2 hours 0.8€/hour, afterwards 1€/hour.
- Belval Plaza has 2 parking garages.
Price: Between 7:00 - 18:00, the first hour is free and an additional free hour is earned by shopping at a store in Belval Plaza and scanning the parking ticket at the store. Afterwards, 3€/hour until 18:00. After 18:00, the first hour is free, the second hour is 4€ and the rest of the night is 3€/hour.
- Belval-Gare Park&Rail parking garage.
Price: 0.6€/30 min. (from 06:00 to 19:00) and 0.3€/30 min. (from 19:00 to 06:00). Monthly pass can be obtained at the CFL Store for 85€.
- ALDI parking (small parking, convenient for short visit of BT2 and Rouden Eck).
Price: first 90 min free, next hour 2€, afterwards 3€/hour.

![](img/map.jpg)
