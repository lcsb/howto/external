---
mount: general/useful-links
name: "Useful links for living in Luxembourg"
order: 100
tags:
  - general
redirects:
  - general/usefulLinks
  - general:usefulLinks
  - /external/general/usefulLinks/
  - /cards/general:usefulLinks
  - /external/cards/general:usefulLinks
  - /external/external/general/usefulLinks/
---

# Useful links for living in Luxembourg
Here, we compiles a few resources that might facilitate your start in Luxembourg - or also inspire you if you are already living here for a while.
Please note that those are external link and thus the LCSB has no control of the integrity of the content.

- [JustArrived](https://www.justarrived.lu/en/) - a website in both English and French providing you an overview of life, habits and processes in Luxembourgs, also supported with facts and figures.

- [Biegerpakt](https://biergerpakt.zesummeliewen.lu/en/) - a program from the government designed to promote intercultural living together in Luxembourg and particularily useful for people newsly arrived in the country. Information is avaliable in English, French and German.

- [AngloInfo](https://www.angloinfo.com/luxembourg) - a site for expats living in Luxembourg promoting events and activities.

- [EURAXESS](https://www.euraxess.lu) - An initiative launched to promote research careers and facilitate the mobility of researchers across Europe.
